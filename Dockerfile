FROM base/archlinux

RUN pacman -Sy --noconfirm \
    && pacman-key --populate \
    && pacman -S --noconfirm archlinux-keyring \
    && pacman -S --noconfirm fontconfig \
    && pacman -S --noconfirm zsh powerline powerline-fonts ncurses
 
COPY zshrc /root/.zshrc
