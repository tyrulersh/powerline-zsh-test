.PHONY: all base

all: base

base: Dockerfile zshrc
	docker build -t tyrulersh/powerline-zsh-base .
